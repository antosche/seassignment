﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Log;
using FileFindAndDeleteLibrary.MurmurHash2;
using FileFindAndDeleteLibrary.RadixSort;

namespace FileFindAndDeleteLibrary
{
    public class FindAndDelete
    {
        public static FileRecordSequence LogFile { get; set; }
        protected static SequenceNumber previousRecord;

        // Find and Delete using default settings
        // Keep only older file
        // Check every duplicate file type
        public static string FindAndDeleteFiles(string[] folders)
        {
            return FindAndDeleteFiles(folders, "*.*", FindAndDeleteOptions.KeepOlderFileOnly);
        }

        public static string FindAndDeleteFiles(string[] folders, string filters, FindAndDeleteOptions options)
        {
            StringBuilder reportBuilder = new StringBuilder();
            DateTime startedTime = DateTime.Now;
            long totalDuplicateFilesFound = 0;
            long totalFilesDeleted = 0;
            long totalDuplicateFilesFailedToDelete = 0;

            #region Generate Report Head
            reportBuilder.AppendLine("Find and Delete Duplicate Files Report");
            reportBuilder.Append("Date and Time started: ");
            reportBuilder.Append(startedTime.ToShortDateString());
            reportBuilder.Append(", ");
            reportBuilder.AppendLine(startedTime.ToLongTimeString());
            reportBuilder.AppendLine();
            reportBuilder.Append("Total folders to search:");
            reportBuilder.AppendLine(folders.Length.ToString());
            reportBuilder.Append("Find and Delete Options:");
            reportBuilder.AppendLine(Enum.GetName(typeof(FindAndDeleteOptions), options));
            reportBuilder.AppendLine();
            #endregion

            #region Set Up Log File
            if (LogFile == null)
            {
                LogFile = new FileRecordSequence("log.dat", FileAccess.ReadWrite);
                previousRecord = SequenceNumber.Invalid;
            }
            else
            {
                try
                {
                    previousRecord = LogFile.BaseSequenceNumber;
                }
                catch (System.ObjectDisposedException)
                {
                    previousRecord = SequenceNumber.Invalid;
                }
            }
            #endregion

            #region Get all files in folders after filtering
            List<FileInfo> allFiles = new List<FileInfo>();
            char[] splitChar = new char[1] { ';' };
            string[] filterStringArray = filters.Split(splitChar, StringSplitOptions.RemoveEmptyEntries);

            foreach (string folder in folders)
            {
                allFiles.AddRange(GetFilesFromDirectory(folder, filterStringArray));
            }
            #endregion

            #region Generate Report Files Found
            reportBuilder.Append("Total files to search: ");
            reportBuilder.AppendLine(allFiles.Count.ToString());
            #endregion

            // Use Radix Sort by Size
            allFiles = Radix.RadixSort(allFiles);

            #region Generate Report Files Found
            reportBuilder.Append("Files to search organised at: ");
            DateTime sortDateTime = DateTime.Now;
            reportBuilder.AppendLine(sortDateTime.ToShortTimeString());
            reportBuilder.Append("Time taken: ");
            reportBuilder.Append((sortDateTime - startedTime).TotalSeconds);
            reportBuilder.AppendLine("seconds");
            reportBuilder.AppendLine();
            #endregion


            // Start taking groups of files with the same file size from allFiles
            while (allFiles.Count != 0)
            {
                // We will get all files with this size
                long sizeToConsider = allFiles[0].Length;

                // local variables to obtain files to consider
                bool stillSameSize = true;
                List<FileInfo> filesToConsider = new List<FileInfo>();
                FileInfo[] filesToConsiderArray;

                // get the first files that match the same size
                while (stillSameSize)
                {
                    if (allFiles[0].Length == sizeToConsider)
                    {
                        filesToConsider.Add(allFiles[0]);
                        allFiles.RemoveAt(0);

                        // if no files are left, then done
                        if (allFiles.Count == 0)
                        {
                            stillSameSize = false;
                        }
                    }
                    else
                    {
                        // as soon as file size changes, we stop (we know we have a sorted array)
                        stillSameSize = false;
                    }
                    //Too slow
                    //IEnumerable<FileInfo> filesToConsider = allFiles.Where(f => f.Length == sizeToConsider);
                }

                #region Build File Size Report
                reportBuilder.AppendLine("Found " + filesToConsider.Count + " files of size " + sizeToConsider + "bytes");
                reportBuilder.AppendLine();
                #endregion

                // only one or zero files found
                if (filesToConsider.Count <= 1)
                {
                    // we cannot find duplicate files here!
                    continue;
                }

                filesToConsiderArray = filesToConsider.ToArray<FileInfo>();
                // should default to false
                bool[] filesMarkedForDeletion = new bool[filesToConsiderArray.Length];

                UInt32?[] murmurHashes = new UInt32?[filesToConsider.Count];
                MurmurHash2Unsafe murmurHasher = new MurmurHash2Unsafe();

                // Calculate a quick hash to avoid pairwise scanning if possible
                for (int i = 0; i < filesToConsiderArray.Length; i++)
                {
                    // open file i and generate its hash!
                    using (FileStream fs = filesToConsiderArray[i].OpenRead())
                    {
                        // Generate a hash only on the first 2048 bytes and the last 2048 bytes -
                        // This will speed up hashing large files
                        // We still read the file byte-wise to check for duplicates - this avoids problems if the start
                        // and end of the file match
                        byte[] fileByte = new byte[Math.Min(4096, fs.Length)];

                        if (fs.Length <= 4096)
                        {
                            fs.Read(fileByte, 0, (int)fs.Length);
                        }
                        else
                        {
                            fs.Read(fileByte, 0, 2048);
                            fs.Seek(-2048, SeekOrigin.End);
                            fs.Read(fileByte, 2048, 2048);
                        }

                        murmurHashes[i] = murmurHasher.Hash(fileByte);
                    }
                }

                List<UniqueFileRepresentation> uniqueFileRepresentations = new List<UniqueFileRepresentation>();

                // Represents the uniqueFileRepresentation in which the file belongs
                Dictionary<string, UniqueFileRepresentation> fileInUniqueRepresentation = new Dictionary<string, UniqueFileRepresentation>();

                // for every pair of files in filesToConsider
                for (int i = 0; i < filesToConsiderArray.Length - 1; i++)
                {
                    // file will already be deleted, ignore (we will already have compared with the other ones)
                    if (filesMarkedForDeletion[i])
                    {
                        continue;
                    }

                    FileInfo firstFile = filesToConsiderArray[i];

                    for (int j = i + 1; j < filesToConsiderArray.Length; j++)
                    {
                        // file will already be deleted
                        if (filesMarkedForDeletion[j])
                        {
                            continue;
                        }

                        FileInfo secondFile = filesToConsiderArray[j];

                        // firstFile and secondFile are potentially identical
                        // We must compare!

                        // if hash matches, scan byte-by-byte for a match!
                        if (murmurHashes[i] == murmurHashes[j])
                        {
                            // hashes have infact matched!
                            // start byte-wise comparison of the two files

                            long bytesToRead = firstFile.Length;
                            bool identical = true;

                            using (FileStream fs1 = firstFile.OpenRead())
                            {
                                using (FileStream fs2 = secondFile.OpenRead())
                                {

                                    while (bytesToRead > 0)
                                    {
                                        if (fs1.ReadByte() != fs2.ReadByte())
                                        {
                                            // not identical
                                            identical = false;
                                            break;
                                        }

                                        bytesToRead--;
                                    }
                                }
                            }

                            if (identical)
                            {
                                string firstFileCompletePath =
                                    firstFile.DirectoryName + firstFile.Name;
                                string secondFileCompletePath =
                                    secondFile.DirectoryName + secondFile.Name;

                                // check whether the files are actually the same exact file!
                                if ((firstFileCompletePath)
                                    == (secondFileCompletePath))
                                {
                                    // same file, do nothing!
                                }
                                else
                                {
                                    #region Generate File Duplicate Report
                                    reportBuilder.AppendLine("The following files are exact copies:");
                                    reportBuilder.AppendLine(firstFileCompletePath);
                                    reportBuilder.AppendLine(secondFileCompletePath);

                                    #region Generate File Duplicate Report
                                    reportBuilder.AppendLine(firstFileCompletePath + " was last modified on " + firstFile.LastWriteTimeUtc.ToShortDateString() + ", " + firstFile.LastWriteTimeUtc.ToShortTimeString() + " UTC");
                                    reportBuilder.AppendLine(secondFileCompletePath + " was last modified on " + secondFile.LastWriteTimeUtc.ToShortDateString() + ", " + secondFile.LastWriteTimeUtc.ToShortTimeString() + " UTC");
                                    #endregion

                                    #endregion

                                    if (fileInUniqueRepresentation.ContainsKey(firstFileCompletePath))
                                    {
                                        if (fileInUniqueRepresentation.ContainsKey(secondFileCompletePath))
                                        {
                                            // both in unique representation
                                            if (fileInUniqueRepresentation[firstFileCompletePath]
                                                == fileInUniqueRepresentation[secondFileCompletePath])
                                            {
                                                // same unique representation

                                                // ToDo:
                                                // Check:
                                                // Do Nothing?
                                            }
                                            else
                                            {
                                                // Unite both representations
                                                // ToDo:
                                                // How?
                                                throw new NotImplementedException();
                                            }
                                        }
                                        else
                                        {
                                            // second file to add in the first file's representation
                                            fileInUniqueRepresentation[firstFileCompletePath].AddFile(secondFile);
                                            fileInUniqueRepresentation.Add(secondFileCompletePath, fileInUniqueRepresentation[firstFileCompletePath]);
                                        }
                                    }
                                    else
                                    {
                                        if (fileInUniqueRepresentation.ContainsKey(secondFileCompletePath))
                                        {
                                            // first file to add in the second file's representation
                                            fileInUniqueRepresentation[secondFileCompletePath].AddFile(firstFile);
                                            fileInUniqueRepresentation.Add(firstFileCompletePath, fileInUniqueRepresentation[secondFileCompletePath]);
                                        }
                                        else
                                        {
                                            // New Unique Representation
                                            UniqueFileRepresentation newUniqueRepresentation =
                                                new UniqueFileRepresentation();
                                            uniqueFileRepresentations.Add(newUniqueRepresentation);

                                            newUniqueRepresentation.AddFile(firstFile);
                                            newUniqueRepresentation.AddFile(secondFile);

                                            fileInUniqueRepresentation.Add(firstFileCompletePath, newUniqueRepresentation);
                                            fileInUniqueRepresentation.Add(secondFileCompletePath, newUniqueRepresentation);
                                        }
                                    }

                                    reportBuilder.AppendLine();
                                }
                            }
                        }
                    }
                }

                // delete all files that have should be deleted!
                foreach (UniqueFileRepresentation ufr in uniqueFileRepresentations)
                {
                    totalDuplicateFilesFound += ufr.SortedDuplicateFileList.Count - 1;

                    #region Report which file should NOT be deleted
                    FileInfo fileToNotDelete = null;

                    switch (options)
                    {
                        case FindAndDeleteOptions.KeepOlderFileOnly:
                            #region Generate File Duplicate Report
                            fileToNotDelete = ufr.SortedDuplicateFileList.Values[0];
                            #endregion
                            break;
                        case FindAndDeleteOptions.KeepNewerFileOnly:
                            #region Generate File Duplicate Report
                            fileToNotDelete = ufr.SortedDuplicateFileList.Values[ufr.SortedDuplicateFileList.Count - 1];
                            #endregion
                            break;
                    }

                    if (fileToNotDelete != null)
                    {
                        string fileToNotDeletePath = fileToNotDelete.DirectoryName + fileToNotDelete.Name;
                        reportBuilder.AppendLine(fileToNotDeletePath + " is the oldest copy of this file.");
                    }
                    #endregion

                    // Default settings - for report only
                    bool deleteFiles = false;
                    int startCounter = 0;
                    int endCounter = ufr.SortedDuplicateFileList.Count;

                    switch (options)
                    {
                        case FindAndDeleteOptions.KeepOlderFileOnly:
                            #region Delete Newest File
                            deleteFiles = true;
                            startCounter = 1;
                            endCounter = ufr.SortedDuplicateFileList.Count;
                            #endregion
                            break;
                        case FindAndDeleteOptions.KeepNewerFileOnly:
                            #region Delete Oldest File
                            deleteFiles = true;
                            startCounter = 0;
                            endCounter = ufr.SortedDuplicateFileList.Count - 1;
                            #endregion
                            break;
                    }

                    for (int i = startCounter; i < endCounter; i++)
                    {
                        string action = "Duplicate File Found";

                        if (deleteFiles == true)
                        {
                            action = "Deleting";
                        }
                        #region Generate Delete File Report
                        // Writes records to the log file.
                        previousRecord = LogFile.Append(CreateData(DateTime.Now.ToShortDateString() + ", " + DateTime.Now.ToShortTimeString() + ": " + action + " " + ufr.SortedDuplicateFileList.Values[i].FullName), SequenceNumber.Invalid, previousRecord, RecordAppendOptions.ForceFlush);

                        reportBuilder.Append(ufr.SortedDuplicateFileList.Values[i].DirectoryName + ufr.SortedDuplicateFileList.Values[i].Name);
                        #endregion

                        if (deleteFiles == true)
                        {
                            try
                            {
                                // Delete the file!
                                ufr.SortedDuplicateFileList.Values[i].Delete();

                                totalFilesDeleted++;

                                #region Generate Delete File Report
                                reportBuilder.AppendLine(" was deleted");
                                #endregion

                                /// Writes records to the log file.
                                previousRecord = LogFile.Append(CreateData(DateTime.Now.ToShortDateString() + ", " + DateTime.Now.ToShortTimeString() + ": " + ufr.SortedDuplicateFileList.Values[i].FullName + " was deleted Succesfully!"), SequenceNumber.Invalid, previousRecord, RecordAppendOptions.ForceFlush);
                            }
                            catch (Exception ex)
                            {
                                // problem deleting file!
                                // Writes records to the log file.
                                previousRecord = LogFile.Append(CreateData(DateTime.Now.ToShortDateString() + ", " + DateTime.Now.ToShortTimeString() + ": Error deleting file " + ufr.SortedDuplicateFileList.Values[i].FullName + ": " + ex.Message), SequenceNumber.Invalid, previousRecord, RecordAppendOptions.ForceFlush);

                                totalDuplicateFilesFailedToDelete++;

                                #region Generate Delete File Report
                                reportBuilder.AppendLine(" failed to delete");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Generate Duplicate Found File Report
                            reportBuilder.AppendLine(" is duplicate");
                            #endregion
                        }
                    }
                }
            }

            #region Generate Report Summary
            reportBuilder.AppendLine();
            reportBuilder.AppendLine("Report Summary:");
            reportBuilder.AppendLine();

            DateTime endTime = DateTime.Now;
            reportBuilder.Append("File Find and Delete Ended: ");
            reportBuilder.Append(endTime.ToShortDateString());
            reportBuilder.Append(", ");
            reportBuilder.AppendLine(endTime.ToShortTimeString());

            reportBuilder.Append("Total Number of Duplicate Files Found: ");
            reportBuilder.AppendLine(totalDuplicateFilesFound.ToString());

            reportBuilder.Append("Total Number of Duplicate Files Deleted: ");
            reportBuilder.AppendLine(totalFilesDeleted.ToString());

            reportBuilder.Append("Total Number of Files Failed to Delete: ");
            reportBuilder.AppendLine(totalDuplicateFilesFailedToDelete.ToString());

            #endregion

            return reportBuilder.ToString();
        }

        private static IList<ArraySegment<byte>> CreateData(string str)
        {
            /// Creates the encoding object.
            Encoding enc = Encoding.Unicode;

            /// Gets bytes for the entry text.
            byte[] textArray = enc.GetBytes(str);

            /// Creates an array segment.
            ArraySegment<byte>[] segments = new ArraySegment<byte>[1];

            /// Populates the array segment
            /// with the text array.
            segments[0] = new ArraySegment<byte>(textArray);

            /// Returns a list of byte array segments
            return Array.AsReadOnly<ArraySegment<byte>>(segments);
        }

        private static IEnumerable<FileInfo> GetFilesFromDirectory(string folder)
        {
            return GetFilesFromDirectory(folder, new string[] {"*.*"});
        }

        private static IEnumerable<FileInfo> GetFilesFromDirectory(string folder, string[] filters)
        {
            List<FileInfo> fileList = new List<FileInfo>();

            foreach (string subDirectory in Directory.GetDirectories(folder, "*", SearchOption.TopDirectoryOnly))
            {
                fileList.AddRange(GetFilesFromDirectory(subDirectory, filters));
            }

            DirectoryInfo currentDir = new DirectoryInfo(folder);

            foreach (string filter in filters)
            {
                fileList.AddRange(currentDir.GetFiles(filter, SearchOption.TopDirectoryOnly));
            }

            return fileList;
        }
    }
}
