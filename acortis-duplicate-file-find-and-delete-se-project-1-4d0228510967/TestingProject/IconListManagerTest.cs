﻿using Etier.IconHelper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Windows.Forms;
using System.Drawing;

namespace TestingProject
{
    
    
    /// <summary>
    ///This is a test class for IconListManagerTest and is intended
    ///to contain all IconListManagerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IconListManagerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion
        private System.Windows.Forms.ImageList imageList = new System.Windows.Forms.ImageList();
        private IconReader.IconSize iconSize = 0;

        /// <summary>
        ///A test for AddExtension
        ///</summary>
        [TestMethod()]
        [DeploymentItem("DuplicateFileFindAndDelete.exe")]
        public void AddExtensionTest()
        {
            
            //actual
            IconListManager target = new IconListManager(imageList, iconSize); // TODO: Initialize to an appropriate value
            string Extension = ".jpg"; // TODO: Initialize to an appropriate value
            int ImageListPosition = 0; // TODO: Initialize to an appropriate value
            target.AddExtension(Extension, ImageListPosition);


            //expected
            Hashtable _extensionExpected = new Hashtable();
            _extensionExpected.Add(Extension, ImageListPosition);
            CollectionAssert.AreEqual(_extensionExpected, target._extensionList);
        }

        [TestMethod()]
        [ExpectedException(typeof (ArgumentNullException))]
        public void AddExtensionTest1()
        {

            //actual
            IconListManager target = new IconListManager(imageList, iconSize); // TODO: Initialize to an appropriate value
            string Extension = null; // TODO: Initialize to an appropriate value
            int ImageListPosition = 0; // TODO: Initialize to an appropriate value
            target.AddExtension(Extension, ImageListPosition);


            //expected
            Hashtable _extensionExpected = new Hashtable();
            _extensionExpected.Add(Extension, ImageListPosition);
            CollectionAssert.AreEqual(_extensionExpected, target._extensionList);
        }

        [TestMethod()]
        [DeploymentItem("DuplicateFileFindAndDelete.exe")]
        public void AddExtensionTest2()
        {
            //actual
            IconListManager target = new IconListManager(imageList, iconSize); // TODO: Initialize to an appropriate value
            string[] Extension ={ ".jpg", ".png", ".txt" }; // TODO: Initialize to an appropriate value
            int ImageListPosition = 0; // TODO: Initialize to an appropriate value
            Hashtable _extensionExpected = new Hashtable();
            for (int i = 0; i < 3; i++)
            {
                target.AddExtension(Extension[i], ImageListPosition);
                _extensionExpected.Add(Extension[i], ImageListPosition);
            }
           
            CollectionAssert.AreEqual(_extensionExpected, target._extensionList);
        }

        [TestMethod()]
        [DeploymentItem("DuplicateFileFindAndDelete.exe")]
        public void AddExtensionTest3()
        {

            //actual
            IconListManager target = new IconListManager(imageList, iconSize); // TODO: Initialize to an appropriate value
            string Extension = ".jpg"; // TODO: Initialize to an appropriate value
            int ImageListPosition = -1; // TODO: Initialize to an appropriate value
            target.AddExtension(Extension, ImageListPosition);


            //expected
            Hashtable _extensionExpected = new Hashtable();
            _extensionExpected.Add(Extension, ImageListPosition);
            CollectionAssert.AreEqual(_extensionExpected, target._extensionList);
        }

        [TestMethod()]
        [DeploymentItem("DuplicateFileFindAndDelete.exe")]
        public void AddExtensionTest4()
        {

            //actual
            IconListManager target = new IconListManager(imageList, iconSize); // TODO: Initialize to an appropriate value
            string Extension = ".jpg"; // TODO: Initialize to an appropriate value
            int ImageListPosition = 1; // TODO: Initialize to an appropriate value
            target.AddExtension(Extension, ImageListPosition);


            //expected
            Hashtable _extensionExpected = new Hashtable();
            _extensionExpected.Add(Extension, ImageListPosition);
            CollectionAssert.AreEqual(_extensionExpected, target._extensionList);
        }

        [TestMethod()]
        [DeploymentItem("DuplicateFileFindAndDelete.exe")]
        public void AddExtensionTest5()
        {

            //actual
            IconListManager target = new IconListManager(imageList, iconSize); // TODO: Initialize to an appropriate value
            string Extension = string.Empty; // TODO: Initialize to an appropriate value
            int ImageListPosition = 1; // TODO: Initialize to an appropriate value
            target.AddExtension(Extension, ImageListPosition);


            //expected
            Hashtable _extensionExpected = new Hashtable();
            _extensionExpected.Add(Extension, ImageListPosition);
            CollectionAssert.AreEqual(_extensionExpected, target._extensionList);
        }

        /// <summary>
        ///A test for AddFileIcon
        ///</summary>
        [TestMethod()]
        public void AddFileIconTest()
        {
            ImageList imageList = new ImageList(); // TODO: Initialize to an appropriate value
            imageList.ImageSize = new Size(255, 255);
            imageList.TransparentColor = Color.White;
            IconReader.IconSize iconSize = new IconReader.IconSize(); // TODO: Initialize to an appropriate value
            IconListManager target = new IconListManager(imageList, iconSize); // TODO: Initialize to an appropriate value
            string filePath = @"..\..\..\TestFolder\yinyan.jpg"; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.AddFileIcon(filePath);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [ExpectedException(typeof(System.IO.FileNotFoundException))]
        public void AddFileIconTest1()
        {
            ImageList imageList = new ImageList(); // TODO: Initialize to an appropriate value
            imageList.ImageSize = new Size(255, 255);
            imageList.TransparentColor = Color.White;
            IconReader.IconSize iconSize = new IconReader.IconSize(); // TODO: Initialize to an appropriate value
            IconListManager target = new IconListManager(imageList, iconSize); // TODO: Initialize to an appropriate value
            string filePath = null; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.AddFileIcon(filePath);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [ExpectedException(typeof(System.IO.FileNotFoundException))]
        public void AddFileIconTest2()
        {
            ImageList imageList = new ImageList(); // TODO: Initialize to an appropriate value
            imageList.ImageSize = new Size(255, 255);
            imageList.TransparentColor = Color.White;
            IconReader.IconSize iconSize = new IconReader.IconSize(); // TODO: Initialize to an appropriate value
            IconListManager target = new IconListManager(imageList, iconSize); // TODO: Initialize to an appropriate value
            string filePath = string.Empty; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.AddFileIcon(filePath);
            Assert.AreEqual(expected, actual);
        }
    }
}
