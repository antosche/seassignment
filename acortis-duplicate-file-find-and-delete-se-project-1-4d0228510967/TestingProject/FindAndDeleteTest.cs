﻿using FileFindAndDeleteLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Collections.Generic;

namespace TestingProject
{


    /// <summary>
    ///This is a test class for FindAndDeleteTest and is intended
    ///to contain all FindAndDeleteTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FindAndDeleteTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetFilesFromDirectory
        ///</summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\data.csv", "data#csv", DataAccessMethod.Sequential), DeploymentItem("TestingProject\\CSV\\data.csv"), DeploymentItem("TestingProject\\data.csv"), TestMethod]
        public void GetFilesFromDirectoryTest()
        {
            string expectedException = Convert.ToString(TestContext.DataRow["Exception"]);

            try
            {
                string folder = TestContext.DataRow["folder"].ToString(); // TODO: Initialize to an appropriate value
                string filterText = Convert.ToString(TestContext.DataRow["filters"]); // TODO: Initialize to an appropriate value
                int arraySize = (int)TestContext.DataRow["ArraySize"];

                string[] filters = new string[arraySize];
                filters = filterText.Split(' ');

                int expected = (int)TestContext.DataRow["expected"]; // TODO: Initialize to an appropriate value
                int actual = 0;

                IEnumerable<FileInfo> data = FindAndDelete_Accessor.GetFilesFromDirectory(folder, filters);

                foreach (FileInfo fi in data)
                {
                    actual++;
                }
                Assert.AreEqual(expected, actual);
                if (!String.IsNullOrWhiteSpace(expectedException))
                {
                    Assert.Fail("Expected a " + expectedException + " but was not thrown");  
                }
            }
            catch (Exception e)
            {
                if (e.GetType().Name.Equals(expectedException))
                { //success

                }
                else
                {
                    Assert.Fail("Unexpected - " + e.GetType().Name + " was thrown");
                }
                    
            }

        }
    }
}
